#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "disk.h"
#include "fs.h"

struct Superblock{
	char signature[8];
	uint16_t numTotalBlocks;
	uint16_t rootDirIndex;
	uint16_t dataBlockIndex;
	uint16_t numDataBlocks;
	uint8_t FATBlocks;
	char padding[4079];
} __attribute__((packed));

typedef struct Superblock *superblock_t;

struct FAT{
	uint16_t *allocTable;
} __attribute__((packed));

typedef struct FAT *fat_t;

struct FileEntry{
	char filename[16];
	uint32_t fileSize;
	uint16_t firstDataIndex;
	uint64_t eightBytePadding;
	uint16_t twoBytePadding;
} __attribute__((packed));

typedef struct FileEntry entry_t;

struct RootDirectory{
	entry_t directory[128];
} __attribute__((packed));

typedef struct RootDirectory *root_t;

struct FS{
	fat_t fat;
	root_t rootdir;
	superblock_t superblock;
	int fatFree;
	int rdirFree;
	int openFiles;
	int countFiles;
};

typedef struct FS *fs_t;

struct FD{
	char filename[16];
	int id;
	size_t offset;
	int dirIndex;
};

typedef struct FD *fd_t;

struct DataBlock{
	uint8_t data[4096];
};

typedef struct DataBlock * block_t;

static fd_t fileDescriptors[32];
static fs_t curFS;


int fs_mount(const char *diskname)
{
	if(curFS){
		fprintf(stderr, "Trying to mount multiple disks. Please unmount first.\n");
		return -1;
	}
	// if possible, open the disk. if not, error.
	if(block_disk_open(diskname))
	{
		fprintf(stderr, "fsmount\n");
		return -1;
	}
	// allocate space for a new file system struct
	fs_t newFS = (fs_t)malloc(sizeof(struct FS));
	// initialize all internal counts to zero, to be set later.
	newFS->rdirFree = 0;
	newFS->fatFree = 0;
	newFS->openFiles = 0;
	newFS->countFiles = 0;
	// allocate space for pieces of file system.	
	superblock_t newSuper = (superblock_t)malloc(4096);
	fat_t newFat = (fat_t)malloc(sizeof(struct FAT));
	root_t newRoot = (root_t)malloc(sizeof(struct RootDirectory));

	// populate the superblock by reading from 0th block.
	if(block_read(0,(void *)newSuper)){
		fprintf(stderr, "super block_read()\n");
		return -1;
	}
	
	// allocate space for FAT based on metadata.
	int fatBlocks = newSuper->FATBlocks;
	newFat->allocTable = (uint16_t *)malloc(fatBlocks*2);
	fat_t curFatBlock = newFat;
	int index = 0;

	// check that the signature is a match.
	char signature[9];	
	strcpy(signature, newSuper->signature);
	signature[8] = '\0';
	if(strcmp(signature,"ECS150FS")){
		fprintf(stderr, "Signature incorrect.");
		return -1;
	}

	// initialize fileDescriptors array to nulls.
	for(int i = 0; i < 32; i++){
		fileDescriptors[i] = NULL;	
	}
	// read blocks equal to number of FATblocks, populate FAT.
	// READ FAT
	for(int i = 1; i < fatBlocks+1; i++){
		if(block_read(i,(void*)(curFatBlock->allocTable+index))){
			fprintf(stderr, "fat block_read()");
			return -1;
		}
		index += 4096;
	} 
	// populate the root directory by reading from disk.
	if(block_read(fatBlocks+1,(void*)newRoot->directory)){
		fprintf(stderr,"root block_read()\n");
		return -1;
	}
	// get FS info by iterating through fat and rdir.
	for(int i = 0; i < 128; i++){
		// '\000' is the char denoting 0 the filename of an empty entry.
		if(newRoot->directory[i].filename[0] == '\000'){
			newFS->rdirFree++;
		}
	}
	newFS->countFiles = 128 - newFS->rdirFree;
	for(int i = 0; i < newSuper->numDataBlocks; i++){
		if((newFat->allocTable[i]) == '\000'){
			newFS->fatFree++;
		}
	}
	// update data structures
	newFS->fat = newFat;
	newFS->rootdir = newRoot;
	newFS->superblock = newSuper;
	// place all information in local main data structure.
	curFS = newFS;
	return 0;
}

int fs_umount(void)
{
	// if no disk mounted.
	if(!curFS){
		fprintf(stderr, "No underlying virtual disk currently mounted.\n");
		return -1; 
	}
	// if there are still open file descriptors
	if(curFS->openFiles){
		fprintf(stderr, "There are still open file descriptors.\n");
		return -1;
	}
	// reverse of the superblock read in fs_mount();
	if(block_write(0,(void *)curFS->superblock)){
		fprintf(stderr, "superblock block_write()\n");
		return -1;	
	}
	// reverse process for the fat read in fs_mount();
	int fatBlocks = curFS->superblock->FATBlocks;
	fat_t curFatBlock = curFS->fat;
	int index = 0;
	for(int i = 1; i < fatBlocks+1; i++){
		if(block_write(i,(void*) curFatBlock->allocTable+index)){
			fprintf(stderr, "fat block_write()\n");
			return -1;
		}
		index += 4096;
	}
	// reverse of the rootDirectory read in fs_mount();
	if(block_write((curFS->superblock->FATBlocks)+1,(void*) curFS->rootdir->directory)){
		fprintf(stderr, "rootdir block_write()\n");
		return -1;	
	}
	// close the disk.
	if(block_disk_close()){
		fprintf(stderr, "fs umount, disk cannot be closed.\n");
		return -1;
	}
	// set local main structure to null--no disk mounted.
	curFS = NULL;
	return 0;
}

int fs_info(void)
{
	// check that a disk is mounted
	if(!curFS){
		fprintf(stderr, "Info no disk mounted");
		return -1;
	}
	// and if so, print information from local main data structure.
	printf("FS Info:\n");
	printf("total_blk_count=%d\n",curFS->superblock->numTotalBlocks);
	printf("fat_blk_count=%d\n",curFS->superblock->FATBlocks);
	printf("rdir_blk=%d\n",curFS->superblock->rootDirIndex);	
	printf("data_blk=%d\n",curFS->superblock->dataBlockIndex);
	printf("data_blk_count=%d\n",curFS->superblock->numDataBlocks);
	printf("fat_free_ratio=%d/%d\n",curFS->fatFree, curFS->superblock->numDataBlocks);
	printf("rdir_free_ratio=%d/128\n",curFS->rdirFree);	
	return 0;
}

int fs_create(const char *filename)
{
	int fileLen = strlen(filename);
	// check that @filename is null-terminated and not overly long.
	if(fileLen > 15){
		fprintf(stderr,"fs_create(): Filename is too long.\n");
		return -1;		
	}
	if(curFS->fatFree == 0){
		fprintf(stderr,"No more room in FAT for file.\n");
		return -1;	
	}
	// check that @filename is not in the root directory.
	for(int i = 0; i < 128; i++){
		if(!strcmp(curFS->rootdir->directory[i].filename, filename)){
			printf("Trying to create a file with a name already in the directory.\n");
			return -1;
		}
	}	
	// check that root directory does not contain @file_max_count files.
	// find new place in FAT to place file.
	int firstFatFree = 0;
	for(int i = 0; i < curFS->superblock->numDataBlocks; i++){
		if(curFS->fat->allocTable[i] == 0){
			firstFatFree = i;
			curFS->fat->allocTable[i] = 0xFFFF;
			break;		
		}
	}	
	for(int i = 0; i < 128; i++){
		if(curFS->rootdir->directory[i].filename[0] == '\000'){
			strcpy(curFS->rootdir->directory[i].filename, filename);
			curFS->rootdir->directory[i].fileSize = 0;
			curFS->rootdir->directory[i].firstDataIndex = firstFatFree;
			break;
		}
	}
	// update counters in main data structure.
	curFS->fatFree--;	
	curFS->rdirFree--;
	curFS->countFiles++;
	return 0;
}

int fs_delete(const char *filename)
{
	// @filename is invalid
	int fileLen = strlen(filename);
	if(fileLen > 15){
		fprintf(stderr,"fs_delete(): Filename is too long.\n");
		return -1;		
	}
	// @filename is currently open
	// iterate through root directory and find @filename.	
	for(int i = 0; i < 128; i++){
		if(!strcmp(curFS->rootdir->directory[i].filename, filename)){
			curFS->rootdir->directory[i].filename[0] = '\000';
			curFS->rootdir->directory[i].fileSize = 0;
			int index = curFS->rootdir->directory[i].firstDataIndex;
			int temp;
			// follow follow index pointers and reset to @FAT_EOC.
			do{ 
				temp = curFS->fat->allocTable[index];
				curFS->fat->allocTable[index] = '\000';
				index = temp;
				curFS->fatFree++;
			}while(index != 0xFFFF);
			curFS->rdirFree++;
			curFS->countFiles--;	
			return 0;
		}
	}	
	// if the for loop terminates and filename cannot be found
	fprintf(stderr,"Filename not found for fs_delete().\n");
	return -1;
}

int fs_ls(void)
{
	if(!curFS){
		fprintf(stderr, "No disk mounted for LS.\n");
		return -1;
	}
	printf("FS Ls:\n");
	// iterate through root directory, print info for nonempty entries.
	for(int i = 0; i < 128; i++){
		if(curFS->rootdir->directory[i].filename[0] != '\000'){
		printf("file: %s, size: %d, data_blk: %d\n",curFS->rootdir->directory[i].filename, curFS->rootdir->directory[i].fileSize, curFS->rootdir->directory[i].firstDataIndex);
		}
	}
	return 0;
}

int fs_open(const char *filename)
{
	for(int i = 0; i < 32; i++){
		if(fileDescriptors[i] != NULL){
			printf("File descriptor [%d] is active in fs_open.\n",i);
		}
	}
	// @filename is invalid
	int fileLen = strlen(filename);
	if(fileLen > 15){
		fprintf(stderr,"fs_open(): Filename is too long.\n");
		return -1;		
	}
	// if there are already @open_file_max files open
	if(curFS->openFiles == 32){
		fprintf(stderr, "Too many files open.\n");
		return -1;
	}
	int i;
	// iterate through directory, try to find file
	for(i = 0; i < 128; i++){
		if(!strcmp(curFS->rootdir->directory[i].filename, filename)){
			break;		
		}
	}
	// if file not found
	if((i == 128) && (strcmp(curFS->rootdir->directory[i].filename, filename))){
			fprintf(stderr, "File not found for fs_open.\n");
			return -1;	
	}
	// allocate space for a new file descriptor struct
	fd_t newFD = (fd_t)malloc(sizeof(struct FD));
	// initialize its data.
	strcpy(newFD->filename, filename);
	newFD->offset = 0;
	newFD->dirIndex = i;
	// find an open space in array and place file descriptor struct there.
	for(i = 0; i < 32; i++){
		if(fileDescriptors[i] == NULL){
			newFD->id = i;
			fileDescriptors[i] = newFD;
			break;		
		}
	}
	// update counter in main data structure.
	curFS->openFiles++;
	return newFD->id;
}

int fs_close(int fd)
{
	// check for proper input
	if((fd < 0) || (fd > 31)){
		fprintf(stderr,"File descriptor is out of bounds.\n");
		return -1;
	}
	if(!fileDescriptors[fd]){
		fprintf(stderr,"File descriptor is not currently open.\n");
		return -1;
	}
	// free allocated space
	free(fileDescriptors[fd]);
	// free up entry in file descriptors
	fileDescriptors[fd] = NULL;
	// update counter in main data structure
	curFS->openFiles--;
	return 0;
}

int fs_stat(int fd)
{
	// check for proper input	
	if((fd < 0) || (fd > 32)){
		fprintf(stderr,"File descriptor is out of bounds.\n");
		return -1;
	}
	if(!fileDescriptors[fd]){
		fprintf(stderr,"File descriptor is not currently open.\n");
		return -1;
	}
	// find correct rdirIndex from fd struct
	int index = fileDescriptors[fd]->dirIndex;
	// use index to get size, and return.
	return (int)curFS->rootdir->directory[index].fileSize;
}

int fs_lseek(int fd, size_t offset)
{
	// check for proper input	
	if((fd < 0) || (fd > 32)){
		fprintf(stderr,"File descriptor is out of bounds.\n");
		return -1;
	}
	if(!fileDescriptors[fd]){
		fprintf(stderr,"File descriptor is not currently open.\n");
		return -1;
	}
	if((offset < 0) || (offset > fs_stat(fd))){
		fprintf(stderr,"Offset out of bounds.\n");
		return -1;
	}
	fileDescriptors[fd]->offset = offset;
	return 0;
}

int fs_write(int fd, void *buf, size_t count)
{
	int bytesWritten = 0;
	block_t dataBlock = (block_t)malloc(sizeof(struct DataBlock));
	fd_t curFD = (fd_t)malloc(sizeof(struct FD));
	curFD = fileDescriptors[fd];
	size_t blockOffset = curFD->offset % 4096;
	int dirIndex = curFD->dirIndex;
	int dbIndex = curFS->rootdir->directory[dirIndex].firstDataIndex;
	int dataBlockNumber = curFS->superblock->dataBlockIndex;
	if(block_read((dbIndex+dataBlockNumber),(void*)dataBlock->data)){
		fprintf(stderr, "fs_write block_read() error");
		return -1;
	}
	while(count > 0){
		// case where all of count will fit in current block.
		if(count <= (4096 - blockOffset)){
			memcpy((void*)dataBlock->data, buf, count);
			block_write((dbIndex+dataBlockNumber),(void*)dataBlock->data);
			curFD->offset += count;
			bytesWritten += count;
			count = 0;
		}
		// case where we need to load more data blocks.
		else{
			void *tempBuf = (void*)malloc(4096-blockOffset);
			memcpy(tempBuf, (void*)((dataBlock->data)+blockOffset), (4096-blockOffset));
			strcat(buf, tempBuf);
			// get next block from FAT;
			dbIndex = curFS->fat->allocTable[dbIndex];
			if(block_read((dbIndex+dataBlockNumber),(void*)dataBlock)){
				fprintf(stderr, "fs_read block_read() error");
				return -1;
			}
			curFD->offset += 4096 - blockOffset;
			assert(curFD->offset % 4096 == 0);
			blockOffset = 0;
			count -= (4096 - blockOffset);
			bytesWritten += (4096 - blockOffset);
			free(tempBuf);
		}
	}// end while();
	return bytesWritten;
}

int fs_read(int fd, void *buf, size_t count)
{
	// if file descriptor is invalid
	int bytesRead;
	block_t dataBlock = (block_t)malloc(sizeof(struct DataBlock));
	fd_t curFD = (fd_t)malloc(sizeof(struct FD));
	//void *tempBuf = (void*)malloc(count);
	curFD = fileDescriptors[fd];
	size_t blockOffset = curFD->offset % 4096;
	int dirIndex = curFD->dirIndex;
	int dbIndex = curFS->rootdir->directory[dirIndex].firstDataIndex;
	// read through blocks until offset < 4096;
	int dataBlockNumber = curFS->superblock->dataBlockIndex;	
	if(block_read((dbIndex+dataBlockNumber),(void*)dataBlock->data)){
		fprintf(stderr, "fs_read block_read() error");
		return -1;
	}
	while(count > 0){
		if(count <= (4096 - blockOffset)){
			void *tempBuf = (void*)malloc(count);
			memcpy(tempBuf, (void*)((dataBlock->data)+blockOffset), count);
			strcat(buf, tempBuf);
			curFD->offset += count;
			bytesRead += count;
			count = 0;
			free(tempBuf);
		}
		// case where we need to load more data blocks.
		else{
			void *tempBuf = (void*)malloc(4096-blockOffset);
			memcpy(tempBuf, (void*)((dataBlock->data)+blockOffset), (4096-blockOffset));
			strcat(buf, tempBuf);
			// get next block from FAT;
			dbIndex = curFS->fat->allocTable[dbIndex];
			if(block_read((dbIndex+dataBlockNumber),(void*)dataBlock)){
				fprintf(stderr, "fs_read block_read() error");
				return -1;
			}
			curFD->offset += 4096 - blockOffset;
			assert(curFD->offset % 4096 == 0);
			blockOffset = 0;
			count -= (4096 - blockOffset);
			bytesRead += (4096 - blockOffset);
			free(tempBuf);
		}
	}
	return bytesRead;
}

