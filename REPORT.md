*#ECS150 Project 4 Report*
=========================

*#Introduction*
---------------
Our file system primarily consists of a series of packed and formatted 
structs for each special piece of a file system--superblock, root 
directory, and filesystem. Pointers to each of these is also held in an 
FS struct, which serves as the main data structure for our program. We 
also created DataBlock and File Descriptor structs to hold data for each 
of those objects. 


*##Mounting and Unmounting*
---------------------------
Everytime our system mounts a new disk, it initializes new structs for 
each of the primary components listed above with malloc. It then reads 
them in from the disk with block_read() and updates the data structures 
accordingly. It then iterates through the FAT and rootdirectory to 
update the counters in our FS struct that keep track of how many open 
spaces we have in each of those, as well as how many total files we 
have. Finally, we set our curFS pointer to the newly allocated FS struct 
that holds each of the other structs, allowing it to serve as the 
central data structure. 

To unmount, our system performed the reverse operation for each of 
superblock, FAT, and rootdirectory. It called block_write() and wrote 
each struct back to the virtual disk. Finally, we call 
block_disk_close(), and set curFS to NULL to signify that no disk is 
mounted. 

Finally, FS_Info() checks that a disk is mounted and then prints out 
information from the central data structure and the superblock. 


*###File Creation and Deletion*
-------------------------------
FS_CREATE() begins with checking that the input filename is valid and 
not already present in the root directory, and that the FAT has a free 
space. We then find the first free space in the FAT and set its value to 
0xFFFF, preparing to allocate that spot for the new file. Finally, we 
iterate through the root directory for an open spot, place the new file 
inside, set the corresponding pieces of data, and update the counters in 
our main data struct. 

FS_DELETE() again checks that the input filename is proper. Then, it 
iterates through the root directory to find the file, sets its filename 
to '\000' to free up the spot, and follows the file's FAT chain, freeing 
up each spot in the FAT as it goes along. Finally, we update the data 
structures and return. If, however, we iterate through the entire 
directory without finding the file, we return an error. 

FS_LS() first checks that a disk is mounted. It then iterates through 
the root directory looking for any spot with a filename other than 
'\000', indicating that a file exists there. It then prints the relevant 
information and returns when finished iterating. 


*####File Descriptors*
----------------------
For phase 3, we created a new struct, the FD struct, which holds the 
corresponding file's name, its file descriptor number, its offset, and 
its index in the rootdirectory. Then we created an array of 32 FD struct 
pointers, and initialize them all as NULL. 

When a file is opened with FS_OPEN(), its FD struct is initialized with 
malloc and given the correct information. It then gets the first NULL 
space in the array, and is stored there for future reference. Finally, 
we update the openFiles counter in our central data structure, and 
return the FD's ID. 

For FS_CLOSE(), we check that the input fd is in range [0,31], and then 
check that spot in our file descriptor array. If it's NULL, there was no 
file open with that file descriptor, and we return an error. Otherwise, 
we simply free the space, set the array index to null, and update the 
counter for open files. 

FS_STAT() checks the input fd the same way as the other functions, then 
returns the size of the file, which is stored in the root directory. 

FS_LSEEK() again checks for proper input, then simply sets the offset of 
the correct file descriptor struct to the input offset. 


*#####File Reading/Writing*
----------------------
Our file reading and writing is incomplete. We started with FS_READ(), 
which initialized a dataBlock struct which holds 4096 bytes of memory. 
FS_READ() then retrieved the correct file descriptor struct, and 
initialized the blockOffset variable (which is how far into a block the 
offset is) using modulo. Then we read the data block into our struct, 
and entered our while loop, which continues until we've read the input 
number of bytes. Here, our FS_READ() split into two cases--one where all 
the work is done in the current data block, and one where we need to 
load more data blocks. We got the single block working, and tried to use 
strcat to dynamically build the read data. However, our other case does 
not seem to work, because we could not properly append the data from 
each block to that of previous blocks. The functionality decrements 
count at each iteration of the while loop by the amount of bytes read, 
and increments the file offset by the same amount. 

FS_WRITE() doesn't seem to work at all, though it follows pretty much 
the same logic as FS_READ(). This time, we reversed source and 
destination of memcpy, and then wrote the block back to disk after each 
memcpy. Again, we updated offset and bytesWritten each time, and 
eventually returned bytesWritten. 


